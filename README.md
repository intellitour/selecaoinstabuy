# Entrega para o Processo seletivo desenvolvedor iOS Instabuy #

Olá, pessoa. Na verdade eu não sou um candidato à vaga de estágio. Sou um profissional consolidado na área e busco por uma oportunidade de trabalho freelance. Compreendo que este pode não ser o momento adequado, mas peço encarecidamente que a minha entrega seja avaliada e que meu contato seja guardado com carinho, caso gostem do meu trabalho.

# A entrega #

Na confecção do produto que você está prestes a ver, me preocupei mais com aspectos técnicos do que com aspectos visuais. O app da entrega faz o melhor possível para trazer os dados de forma rápida, assíncrona e tem o cuidado de guardar tudo no CoreData para que seja possível acessar as informações depois, mesmo sem conexão. Entendo que talvez isso não seja interessante para conjunto específico de informações do teste, mas eu julguei interessante implementar tal solução para corroborar com a avaliação das minhas competências técnicas.

# Instruções #
Bom... este é um projeto simples, sem dependências externas, feito no Xcode 8.3.3. Assim sendo, basta abrir o projeto e executar.

# Quem é Pedro? #

Pedro é desenvolvedor iOS desde 2010 quando fez o primeiro aplicativo como TCC no curso de Bacharelado em Sistemas de Informação da Universidade Católica de Brasília. Depois disso, atuou em várias frentes do desenvolvimento mobile, tendo aplicativos de relevância nacional publicados na AppStore, com destaque para os aplicativos da Justiça Eleitoral e para a rede social Thirty: Tente algo novo por 30 dias.

Veja o perfil profissional em: http://linkedin.com/in/intellitour/?locale=pt_BR