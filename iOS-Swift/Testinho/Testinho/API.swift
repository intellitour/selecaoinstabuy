//
//  API.swift
//  Testinho
//
//  Created by Pedro Henrique on 18/09/17.
//
//

import Foundation

struct API {
    private static let baseURL = "https://instabuy.com.br/apiv2_2"
    static let Product = baseURL+"/product.json"
    
    struct Image {
        
        enum Size {
            case Small
            case Medium
            case Big
            case Large
        }
        
        private static var sizes: Dictionary<Size, (String, String)> = [
            Size.Small: ("small", "s"),
            Size.Medium: ("medium", "m"),
            Size.Big: ("big", "b"),
            Size.Large: ("large", "l")
        ]
        
        private static let imageFormat = "https://s3-us-west-2.amazonaws.com/ib.image.%@/%@-%@"
        
        static func url(withSize size: Size, forImageName name: String) -> URL? {
            let (text, letter) = sizes[size]!
            let url = String(format: imageFormat, text, letter, name)
            return URL(string: url)
        }
    }
}

struct ContentType {
    public static let ApplicationJSON = "application/json"
}

struct HTTPHeader {
    public static let Accept = "Accept"
}
