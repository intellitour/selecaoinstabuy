//
//  Alerts.swift
//  Testinho
//
//  Created by Pedro Henrique on 18/09/17.
//
//

import UIKit

extension UIViewController {

    func alert(withTitle title: String, message: String, actions: [UIAlertAction]? = nil) {
        if Thread.current.isMainThread {
            makeAlert(withTitle: title, message: message, actions: actions)
        }else {
            DispatchQueue.main.async { [weak weakSelf = self] in
                weakSelf?.makeAlert(withTitle: title, message: message, actions: actions)
            }
        }
    }
    
    private func makeAlert(withTitle title: String, message: String, actions: [UIAlertAction]? = nil) {
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if let alertActions = actions, alertActions.count > 0 {
            for action in alertActions {
                ac.addAction(action)
            }
        }else {
            let action = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                ac.dismiss(animated: true, completion: nil)
            })
            ac.addAction(action)
        }
        present(ac, animated: true, completion: nil)
    }

}
