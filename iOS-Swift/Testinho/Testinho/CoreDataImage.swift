//
//  CoreDataImage.swift
//  Testinho
//
//  Created by Pedro Henrique on 18/09/17.
//
//

import UIKit

public class CoreDataImage: UIImage {

    /* 
       Esta classe é necessária pois o Xcode não
       consegue importar o UIKit sozinho ao gerar
       as classes/categorias relativas às entidades
       do CoreData.
    */
}
