//
//  Label.swift
//  Testinho
//
//  Created by Pedro Henrique on 18/09/17.
//
//

import Foundation

struct Label: DictionaryInitializable {
    var value : String
    var key : String
    
    init?(dictionary: [String:AnyObject]) {
        guard let pKey = dictionary["key"] as? String else {
            return nil
        }
        guard let pValue = dictionary["value"] as? String else {
            return nil
        }
        key = pKey
        value = pValue
    }
}
