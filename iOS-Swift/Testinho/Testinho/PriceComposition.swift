//
//  PriceComposition.swift
//  Testinho
//
//  Created by Pedro Henrique on 18/09/17.
//
//

import Foundation

struct PriceComposition: DictionaryInitializable {
    
    var internalCode : String
    var installment : Int
    var stock : Int
    var price : Double
    var validPrice : Double
    var expirationPromotion : Int
    var model : String
    var identifier : String
    
    init?(dictionary: [String: AnyObject]) {
        guard let pInternalCode = dictionary["internalCode"] as? String else {
            return nil
        }
        guard let pInstallment = dictionary["installment"] as? Int else {
            return nil
        }
        guard let pQtdStock = dictionary["qtd_stock"] as? Int else {
            return nil
        }
        guard let pPrice = dictionary["price"] as? Double else {
            return nil
        }
        guard let pValidPrice = dictionary["valid_price"] as? Double else {
            return nil
        }
        guard let pExpirationPromotion = dictionary["expirationPromotion"] as? Int else {
            return nil
        }
        guard let pModel = dictionary["model"] as? String else {
            return nil
        }
        guard let pId = dictionary["id"] as? String else {
            return nil
        }
        
        internalCode = pInternalCode
        installment = pInstallment
        stock = pQtdStock
        price = pPrice
        validPrice = pValidPrice
        expirationPromotion = pExpirationPromotion
        model = pModel
        identifier = pId
    }
    
    
}
