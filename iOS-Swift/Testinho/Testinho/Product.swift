//
//  Product.swift
//  Testinho
//
//  Created by Pedro Henrique on 18/09/17.
//
//

import Foundation

struct Product: DictionaryInitializable {
    
    var storeId : String
    var labels : [Label]
    var visible : Bool
    var identifier : String
    var thumb : String
    var service : Bool
    var unitType : String
    var priceCompositions : [PriceComposition]
    var incrementValue : Int
    var variationProducts : [String]
    var modelFrame : String
    var installmentStarting : Int
    var subdomain : String
    var relatedProducts : [String]
    var hasPromotion : Bool
    var productDescription : String
    var brand : String
    var photos : [String]
    var multi : Bool
    var name : String
    var subCategoryId : String
    var priceStarting : Double
    var modelName : String
    
    init?(dictionary: [String:AnyObject]) {
        guard let pStoreId = dictionary["store_id"] as? String else {
            return nil
        }
        guard let pLabels = dictionary["labels"] as? [[String:AnyObject]] else {
            return nil
        }
        guard let pVisible = dictionary["visible"] as? Bool else {
            return nil
        }
        guard let pId = dictionary["id"] as? String else {
            return nil
        }
        guard let pThumb = dictionary["thumb"] as? String else {
            return nil
        }
        guard let pService = dictionary["service"] as? Bool else {
            return nil
        }
        guard let pUnitType = dictionary["unit_type"] as? String else {
            return nil
        }
        guard let pPc = dictionary["pc"] as? [[String:AnyObject]] else {
            return nil
        }
        guard let pIncrementValue = dictionary["increment_value"] as? Int else {
            return nil
        }
        guard let pVariationProducts = dictionary["variation_products"] as? [String] else {
            return nil
        }
        guard let pModelFrame = dictionary["model_frame"] as? String else {
            return nil
        }
        guard let pInstallmentStarting = dictionary["installment_starting"] as? Int else {
            return nil
        }
        guard let pSubdomain = dictionary["subdomain"] as? String else {
            return nil
        }
        guard let pRelatedProducts = dictionary["related_products"] as? [String] else {
            return nil
        }
        guard let pHasPromotion = dictionary["has_promotion"] as? Bool else {
            return nil
        }
        guard let pProductDescription = dictionary["description"] as? String else {
            return nil
        }
        guard let pBrand = dictionary["brand"] as? String else {
            return nil
        }
        guard let pPhotos = dictionary["photos"] as? [String] else {
            return nil
        }
        guard let pMulti = dictionary["multi"] as? Bool else {
            return nil
        }
        guard let pName = dictionary["name"] as? String else {
            return nil
        }
        guard let pSubCategoryId = dictionary["subCategory_id"] as? String else {
            return nil
        }
        guard let pPriceStarting = dictionary["price_starting"] as? Double else {
            return nil
        }
        guard let pModelName = dictionary["model_name"] as? String else {
            return nil
        }
        storeId = pStoreId
        labels = Product.makeLabels(pLabels)
        visible = pVisible
        identifier = pId
        thumb = pThumb
        service = pService
        unitType = pUnitType
        priceCompositions = Product.makePcs(pPc)
        incrementValue = pIncrementValue
        variationProducts = pVariationProducts
        modelFrame = pModelFrame
        installmentStarting = pInstallmentStarting
        subdomain = pSubdomain
        relatedProducts = pRelatedProducts
        hasPromotion = pHasPromotion
        productDescription = pProductDescription
        brand = pBrand
        photos = pPhotos
        multi = pMulti
        name = pName
        subCategoryId = pSubCategoryId
        priceStarting = pPriceStarting
        modelName = pModelName
    }
    
    private static func makeLabels(_ pLabels:[[String:AnyObject]]) -> [Label] {
        var lbs = [Label]()
        for dict in pLabels {
            if let lb = Label(dictionary: dict) {
                lbs.append(lb)
            }
        }
        return lbs
    }
    
    private static func makePcs(_ pPcs:[[String:AnyObject]]) -> [PriceComposition] {
        var pcs = [PriceComposition]()
        for dict in pPcs {
            if let pc = PriceComposition(dictionary: dict) {
                pcs.append(pc)
            }
        }
        return pcs
    }
    
}
