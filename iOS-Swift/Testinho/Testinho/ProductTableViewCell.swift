//
//  ProductTableViewCell.swift
//  Testinho
//
//  Created by Pedro Henrique on 19/09/17.
//
//

import UIKit

class ProductTableViewCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var moreInfoButton: UIButton!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productDescriptionLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    static let reusableIdentifier = "ProductTableViewCell"
    static let nib: UINib = {
        let nib = UINib(nibName: reusableIdentifier, bundle: Bundle.main)
        return nib
    }()
    
    private static let numberFormatter: NumberFormatter = {
        let nf = NumberFormatter()
        nf.numberStyle = .currency
        return nf
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.backgroundColor = UIColor.darkGray
        
        cardView.layer.cornerRadius = 20
        cardView.layer.masksToBounds = true
        
        moreInfoButton.layer.cornerRadius = moreInfoButton.frame.size.height/2
        moreInfoButton.layer.shadowColor = UIColor.black.cgColor
        moreInfoButton.layer.shadowRadius = 20
        moreInfoButton.layer.shadowOffset = CGSize(width: 3, height: 3)
        moreInfoButton.layer.shadowOpacity = 0.3
        
    }
    
    func setup(withProduct product: TestProduct) {
        productImageView.image = product.thumb ?? #imageLiteral(resourceName: "noImage")
        var animationImages = [UIImage]()
        if let thumb = product.thumb {
            animationImages.append(thumb)
        }
        
        if let photos = product.photos {
            for photo in photos {
                if let productPhoto = photo as? Photo, productPhoto.large != nil {
                    animationImages.append(productPhoto.large!)
                }
            }
            productImageView.animationImages = animationImages
            productImageView.animationDuration = TimeInterval(animationImages.count*2)
        }
        productImageView.startAnimating()
        
        productNameLabel.text = product.name ?? product.brand
        var productDescription = "\(product.productDescription ?? "") - \(product.brand ?? "")"
        if let labels = product.labels {
            productDescription.append("\n\n")
            if let productLabel = labels.allObjects[0] as? ProductLabel {
                productDescription.append("\(productLabel.key ?? ""): \(productLabel.value ?? "")")
            }
            productDescriptionLabel.text = productDescription
        }
        
        priceLabel.text = ProductTableViewCell.numberFormatter.string(from: NSNumber(value: product.price))
        
    }
    
    
    
}
