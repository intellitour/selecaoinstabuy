//
//  ProgressView.swift
//  Testinho
//
//  Created by Pedro Henrique on 18/09/17.
//
//

import UIKit

class ProgressView: UIView {
    
    
    @IBOutlet weak var effect: UIVisualEffectView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var label: UILabel!
    
    private func commonInit() {
        isOpaque = false
        autoresizingMask = [.flexibleWidth, .flexibleHeight]
        backgroundColor = UIColor.clear
        layer.allowsGroupOpacity = false
    }
    
    private func prepareOutlets() {
        effect.layer.cornerRadius = 20
        effect.layer.masksToBounds = true
        
        label.font = UIFont.vertrina(ofSize: 18)
        label.textColor = UIColor.white.withAlphaComponent(0.8)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    public class func show() {
        show(addedToView: UIApplication.shared.keyWindow)
    }
    
    public class func show(addedToView view: UIView?) {
        DispatchQueue.main.async {
            if let v = view {
                let nib = UINib.init(nibName: "ProgressView", bundle: Bundle.main)
                let hud = nib.instantiate(withOwner: nil, options: nil)[0] as! ProgressView
                hud.frame = v.frame
                hud.prepareOutlets()
                
                hud.activityIndicator.startAnimating()
                hud.center = v.center
                v.addSubview(hud)
            }
        }
    }
    
    public class func hide() {
        hide(fromView: UIApplication.shared.keyWindow)
    }
    
    public class func hide(fromView view: UIView?) {
        DispatchQueue.main.async {
            if let hud = progressView(forView: view) {
                hud.activityIndicator.stopAnimating()
                hud.removeFromSuperview()
            }
        }
    }
    
    private class func progressView(forView view: UIView?) -> ProgressView? {
        if let subviews = view?.subviews.reversed() {
            for subview in subviews {
                if subview.isKind(of: ProgressView.self) {
                    let hud = subview as! ProgressView
                    return hud
                }
            }
        }
        return nil
    }
}
