//
//  Response.swift
//  Testinho
//
//  Created by Pedro Henrique on 18/09/17.
//
//

import Foundation

protocol DictionaryInitializable {
    init?(dictionary: [String:AnyObject])
}

struct Response<T:DictionaryInitializable>: DictionaryInitializable {
    
    var type: String
    var status: String
    var data: [T]
    
    init?(dictionary: [String:AnyObject]) {
        guard let pType = dictionary["type"] as? String else {
            return nil
        }
        guard let pStatus = dictionary["status"] as? String else {
            return nil
        }
        guard let pData = dictionary["data"] as? [[String:AnyObject]] else {
            return nil
        }
        
        type = pType
        status = pStatus
        data = [T]()
        for pd in pData {
            if let t = T(dictionary: pd) {
                data.append(t)
            }
        }
    }
    
}
