//
//  TestProduct.swift
//  Testinho
//
//  Created by Pedro Henrique on 18/09/17.
//
//

import CoreData

public class TestProduct: NSManagedObject {
    
    static func createOrUpdate(testProduct product: Product, withContext context: NSManagedObjectContext) throws -> TestProduct {
        var testProduct: TestProduct? = nil
        
        let request: NSFetchRequest<TestProduct> = TestProduct.fetchRequest()
        request.predicate = NSPredicate(format: "identifier = %@", product.identifier)
        
        do {
            let result = try context.fetch(request)
            if result.count == 1 {
                testProduct = result[0]
            }
            if result.count > 1 {
                throw NSError(domain: "Integridade", code: 1, userInfo: ["result": result, "product": product])
            }
        }catch {
            debugPrint(error)
        }
        
        
        if testProduct == nil {
            testProduct = TestProduct(context: context)
        }
        
        testProduct?.identifier = product.identifier
        testProduct?.name = product.name
        testProduct?.brand = product.brand
        testProduct?.productDescription = product.productDescription
        if product.priceCompositions.count > 0 {
            testProduct?.price = product.priceCompositions[0].validPrice
        }
        
        if testProduct?.photos == nil {
            testProduct?.photos = NSMutableSet()
        }else {
            testProduct?.removeFromPhotos(testProduct!.photos!)
        }
        
        if testProduct?.labels == nil {
            testProduct?.labels = NSMutableSet()
        }else {
            testProduct?.removeFromLabels(testProduct!.labels!)
        }
        
        for lb in product.labels {
            let label = ProductLabel(context: context)
            label.key = lb.key
            label.value = lb.value
            testProduct?.addToLabels(label)
        }
        
        loadImages(forProduct: product, onTestProduct: testProduct!)
        return testProduct!
    }
    
    private static func loadImages(forProduct product: Product, onTestProduct testProduct: TestProduct) {
        if let context = testProduct.managedObjectContext {
            let queue = OperationQueue()
            queue.qualityOfService = .userInteractive
            queue.maxConcurrentOperationCount = ProcessInfo.processInfo.processorCount
            
            let thumbOp = BlockOperation {
                context.perform {
                    if let data = try? Data(contentsOf: API.Image.url(withSize: .Large, forImageName: product.thumb)!) {
                        testProduct.thumb = CoreDataImage(data: data)
                    }
                }
            }
            var imgOps = [BlockOperation]()
            for p in product.photos {
                let photo = Photo(context: context)
                testProduct.addToPhotos(photo)
                imgOps.append(BlockOperation(block: {
                    context.perform {
                        if let smallData = try? Data(contentsOf: API.Image.url(withSize: .Small, forImageName: p)!) {
                            photo.small = CoreDataImage(data: smallData)
                        }
                    }
                }))
                imgOps.append(BlockOperation(block: {
                    context.perform {
                        if let mediumData = try? Data(contentsOf: API.Image.url(withSize: .Medium, forImageName: p)!) {
                            photo.medium = CoreDataImage(data: mediumData)
                        }
                    }
                }))
                imgOps.append(BlockOperation(block: {
                    context.perform {
                        if let bigData = try? Data(contentsOf: API.Image.url(withSize: .Big, forImageName: p)!) {
                            photo.big = CoreDataImage(data: bigData)
                        }
                    }
                }))
                imgOps.append(BlockOperation(block: {
                    context.perform {
                        if let largeData = try? Data(contentsOf: API.Image.url(withSize: .Large, forImageName: p)!) {
                            photo.large = CoreDataImage(data: largeData)
                        }
                    }
                }))
            }
            imgOps.append(thumbOp)
            
            queue.addOperations(imgOps, waitUntilFinished: true)
            context.perform {
                try? context.save()
            }
        }
    }
    
    
}
