//
//  TestinhoTableViewController.swift
//  Testinho
//
//  Created by Pedro Henrique on 18/09/17.
//
//

import UIKit
import CoreData

class TestinhoTableViewController: UITableViewController {
    
    fileprivate var appendingData = Data()
    fileprivate var persistentContainer = AppDelegate.persistentContainer
    
    private var showsProgressView = true
    
    private lazy var session: URLSession = { [weak self] in
        let cfg = URLSessionConfiguration.default
        cfg.allowsCellularAccess = true
        cfg.networkServiceType = .default
        cfg.requestCachePolicy = .returnCacheDataElseLoad
        cfg.isDiscretionary = true
        cfg.urlCache = URLCache(memoryCapacity: 0, diskCapacity: 5192, diskPath: NSTemporaryDirectory())
        
        let queue = OperationQueue()
        queue.qualityOfService = .userInteractive
        queue.underlyingQueue = DispatchQueue.global(qos: .userInteractive)
        
        let session = URLSession(configuration: cfg,
                                 delegate: self,
                                 delegateQueue: queue)
        return session
    }()
    
    private lazy var fetchRequest: NSFetchRequest<TestProduct> = {
        let request: NSFetchRequest<TestProduct> = TestProduct.fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        return request
    }()
    
    private lazy var fetchedResultsController: NSFetchedResultsController<TestProduct> = { [unowned self] in
        
        let frc = NSFetchedResultsController(fetchRequest: self.fetchRequest,
                                             managedObjectContext: self.persistentContainer.viewContext,
                                             sectionNameKeyPath: nil,
                                             cacheName: nil)
        frc.delegate = self
        return frc
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.backgroundColor = UIColor.darkGray
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        
        tableView.register(ProductTableViewCell.nib, forCellReuseIdentifier: ProductTableViewCell.reusableIdentifier)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(onContextDidSave(_:)),
                                               name: Notification.Name.NSManagedObjectContextDidSave,
                                               object: nil)
        
        persistentContainer.performBackgroundTask { [unowned self] (context) in
            if let count = try? context.count(for: self.fetchRequest) {
                self.showsProgressView = !(count > 0)
            }else {
                self.showsProgressView = true
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func onContextDidSave(_ notification: Notification) {
        DispatchQueue.main.sync { [unowned self] in
            self.persistentContainer.viewContext.mergeChanges(fromContextDidSave: notification)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        do {
            try self.fetchedResultsController.performFetch()
            self.tableView.reloadData()
        }catch {
            self.alert(withTitle: "Erro ao realizar Fetch", message: error.localizedDescription)
            debugPrint(error)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        var productURL = API.Product
        productURL.append("?subcategory_id=57eec92f072d415b67c24175")

        if let url = URL(string: productURL) {
            var request = URLRequest(url: url)
            request.setValue(ContentType.ApplicationJSON, forHTTPHeaderField: HTTPHeader.Accept)
            
            
            let dataTask = session.dataTask(with: request)
            if showsProgressView {
                ProgressView.show()
            }
            dataTask.resume()
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController.sections?.count ?? 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = fetchedResultsController.sections, sections.count > 0 {
            return sections[section].numberOfObjects
        }else {
            return 0
        }
    }
    
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return fetchedResultsController.sectionIndexTitles
    }
    
    override func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        return fetchedResultsController.section(forSectionIndexTitle: title, at: index)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ProductTableViewCell.reusableIdentifier, for: indexPath) as! ProductTableViewCell
        
        let product = fetchedResultsController.object(at: indexPath)
        cell.setup(withProduct: product)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 486
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

extension TestinhoTableViewController : URLSessionDataDelegate {
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        appendingData.append(data)
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        ProgressView.hide()
        if let e = error {
            print("\n\nErro ao realizar requisição:\n")
            debugPrint(e)
            appendingData.removeAll()
        }else {
            if let response = task.response as? HTTPURLResponse, response.statusCode == 200 {
                do {
                    if let products = try JSONSerialization.jsonObject(with: appendingData, options: .allowFragments) as? [String:AnyObject] {
                        if let apiResponse = Response<Product>(dictionary: products), apiResponse.status == "success", apiResponse.type == "list" {
                            persistentContainer.performBackgroundTask({ (context) in
                                do {
                                    for p in apiResponse.data {
                                        _ = try TestProduct.createOrUpdate(testProduct: p, withContext: context)
                                    }
                                    try context.save()
                                }catch {
                                    debugPrint(error)
                                }
                            })
                        }
                    }
                }catch {
                    self.alert(withTitle: "Ocorreu um erro", message: "Detalhe do erro:\n\(error.localizedDescription)")
                    debugPrint(error)
                }
            }
        }
    }
}

extension TestinhoTableViewController : NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange sectionInfo: NSFetchedResultsSectionInfo,
                    atSectionIndex sectionIndex: Int,
                    for type: NSFetchedResultsChangeType) {
        
        switch type {
            case .insert:
                tableView.insertSections([sectionIndex], with: .bottom)
            case .delete:
                tableView.deleteSections([sectionIndex], with: .bottom)
            case .update:
                tableView.reloadSections([sectionIndex], with: .automatic)
            default:
                break
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange anObject: Any,
                    at indexPath: IndexPath?,
                    for type: NSFetchedResultsChangeType,
                    newIndexPath: IndexPath?) {
        
        
        switch type {
            case .insert:
                tableView.insertRows(at: [newIndexPath!], with: .left)
            case .delete:
                tableView.deleteRows(at: [indexPath!], with: .left)
            case .update:
                tableView.reloadRows(at: [indexPath!], with: .fade)
            case .move:
                tableView.moveRow(at: indexPath!, to: newIndexPath!)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
}
