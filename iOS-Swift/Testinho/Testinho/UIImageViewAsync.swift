//
//  UIImageViewAsync.swift
//  Testinho
//
//  Created by Pedro Henrique on 18/09/17.
//
//

import UIKit

class UIImageViewAsync: UIImageView {

    var imageURL: URL? {
        didSet {
            image = #imageLiteral(resourceName: "noImage")
            DispatchQueue.global(qos: .background).async { [weak self] in
                if let weakSelf = self, weakSelf.imageURL != nil {
                    
                    let queue = OperationQueue()
                    queue.qualityOfService = .userInteractive
                    queue.underlyingQueue = DispatchQueue.global(qos: .userInteractive)
                    
                    let session = URLSession(configuration: URLSessionConfiguration.ephemeral, delegate: nil, delegateQueue: queue)
                    
                    let task = session.dataTask(with: weakSelf.imageURL!) { (data, response, error) in
                        if let imageData = data, error == nil {
                            if let image = UIImage(data: imageData) {
                                DispatchQueue.main.async {
                                    weakSelf.image = image
                                }
                            }else {
                                if error != nil {
                                    print("\n\nErro ao baixar imagem:\n")
                                    debugPrint(error!)
                                }
                            }
                        }
                    }
                    task.resume()
                }
            }
        }
    }

}
