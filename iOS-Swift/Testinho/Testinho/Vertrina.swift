//
//  Vertrina.swift
//  Testinho
//
//  Created by Pedro Henrique on 18/09/17.
//
//

import UIKit

extension UIFont {
    class var vertrinaFontName: String {
        get {
            return "VertrinaCondensed-CondensedBold"
        }
    }
    
    class func vertrina(ofSize size: CGFloat) -> UIFont {
        guard let font =  UIFont(name: vertrinaFontName, size: size) else {
            return UIFont.systemFont(ofSize: size)
        }
        return font
    }
}
